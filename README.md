An Internet Draft about "Encrypted E-mail with Cleartext Copies"
================================================================

This repository hosts the source for [draft-dkg-mail-cleartext-copy](https://datatracker.ietf.org/doc/draft-dkg-mail-cleartext-copy/).
